package projection

import (
	"math"

	"gitee.com/giserhan/turf-go/helpers/types/geojson"
	geojsontypes "gitee.com/giserhan/turf-go/helpers/types/geojson_types"
)

/**
 * Convert 900913 x/y values to lon/lat.
 * 将900913  x/y 坐标转为 经/纬度
 */
func ConvertToWgs84(xy []float64) []float64 {
	//角度转弧度系数
	R2D := 180 / math.Pi
	//地球半径
	R := 6378137.0
	return []float64{
		(xy[0] * R2D) / R,
		(math.Pi*0.5 - 2.0*math.Atan(math.Exp(-xy[1]/R))) * R2D,
	}
}

/**
 * Convert lon/lat values to 900913 x/y.
 * 将wgs84坐标转为Mercator
 */
func ConvertToMercator(lonLat []float64) []float64 {
	D2R := math.Pi / 180
	A := 6378137.0
	MAXEXTENT := 20037508.342789244
	var adjusted float64
	if math.Abs(lonLat[0]) <= 180 {
		adjusted = lonLat[0]
	} else {
		adjusted = lonLat[0] - sign(lonLat[0])*360
	}
	xy := []float64{
		A * adjusted * D2R,
		A * math.Log(math.Tan(math.Pi*0.25+0.5*lonLat[1]*D2R)),
	}
	if xy[0] > MAXEXTENT {
		xy[0] = MAXEXTENT
	}
	if xy[0] < (-MAXEXTENT) {
		xy[0] = -MAXEXTENT
	}
	if xy[1] > MAXEXTENT {
		xy[1] = MAXEXTENT
	}
	if xy[1] < (-MAXEXTENT) {
		xy[1] = -MAXEXTENT
	}
	return xy
}

func sign(x float64) float64 {
	if x < 0 {
		return -1
	}
	if x > 0 {
		return 1
	}
	return 0
}

func ToWGS84(data geojson.GeoJSON) (geojson.GeoJSON, error) {
	return geojson.GeoJSON{
		Type: geojsontypes.Point,
		Coordinates: []float64{
			10, 10,
		},
	}, nil
}
