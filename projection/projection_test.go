package projection_test

import (
	"testing"

	"gitee.com/giserhan/turf-go/projection"
)

func TestXxx(t *testing.T) {
	wgs84 := projection.ConvertToWgs84([]float64{
		13677191.066296693, 3344906.7370524644,
	})
	mercator := projection.ConvertToMercator([]float64{
		122.86429778677294, 28.758204523167176,
	})

	t.Fatalf("%v,%v", wgs84, mercator)
}
