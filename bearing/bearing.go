package bearing

import (
	"math"

	"gitee.com/giserhan/turf-go/helpers"
)

func Bearing(start []float64, end []float64, final bool) float64 {
	if final {
		return calculateFinalBearing(start, end)
	}
	coordinates1 := start
	coordinates2 := end
	lon1 := helpers.DegreesToRadians(coordinates1[0])
	lon2 := helpers.DegreesToRadians(coordinates2[0])
	lat1 := helpers.DegreesToRadians(coordinates1[1])
	lat2 := helpers.DegreesToRadians(coordinates2[1])
	a := math.Sin(lon2-lon1) * math.Cos(lat2)
	b := math.Cos(lat1)*math.Sin(lat2) -
		math.Sin(lat1)*math.Cos(lat2)*math.Cos(lon2-lon1)
	return helpers.RadiansToDegrees(math.Atan2(a, b))
}

func calculateFinalBearing(start []float64, end []float64) float64 {
	bear := Bearing(end, start, false)
	bear = helpers.Remain(bear+180, 360)
	return bear
}
