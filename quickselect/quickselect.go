package quickselect

import "math"

func QuickSelect(arr []float64, k float64, left float64, right float64, compare func(a float64, b float64) int) {
	quickselectStep(arr, k, left, right, compare)
}

func quickselectStep(arr []float64, k float64, left float64, right float64, compare func(a float64, b float64) int) {

	for right > left {
		if right-left > 600 {
			n := right - left + 1
			m := k - left + 1
			z := math.Log(n)
			s := 0.5 * math.Exp(2*z/3)
			fac := m - n/2
			if fac < 0 {
				fac = -1
			} else {
				fac = 1
			}
			sd := 0.5 * math.Sqrt(z*s*(n-s)/n) * fac
			newLeft := math.Max(left, math.Floor(k-m*s/n+sd))
			newRight := math.Min(right, math.Floor(k+(n-m)*s/n+sd))
			quickselectStep(arr, k, newLeft, newRight, compare)
		}
		t := arr[int(k)]
		i := left
		j := right

		swap(arr, left, k)
		if compare(arr[int(right)], t) > 0 {
			swap(arr, left, right)
		}
		for i < j {
			swap(arr, i, j)
			i++
			j--
			for compare(arr[int(i)], t) < 0 {
				i++
			}
			for compare(arr[int(j)], t) > 0 {
				j--
			}
		}
		if compare(arr[int(left)], t) == 0 {
			swap(arr, left, j)
		} else {
			j++
			swap(arr, j, right)
		}
		if j <= k {
			left = j + 1
		}
		if k <= j {
			right = j - 1
		}
	}
}

func swap(arr []float64, i float64, j float64) {
	var tmp = arr[int(i)]
	arr[int(i)] = arr[int(j)]
	arr[int(j)] = tmp
}
