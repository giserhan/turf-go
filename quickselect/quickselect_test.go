package quickselect_test

import (
	"testing"

	"gitee.com/giserhan/turf-go/quickselect"
)

func TestMain(t *testing.T) {
	// t.Fatal(helpers.Round(1.2324242, 2))
	arr := []float64{
		65, 28, 59, 33, 21, 56, 22, 95, 50, 12, 90, 53, 28, 77, 39,
	}
	quickselect.QuickSelect(arr, 8, 0, float64(len(arr))-1, func(a, b float64) int {
		if a < b {
			return -1
		} else if a > b {
			return 1
		}
		return 0
	})
	t.Fatal(arr)
}
