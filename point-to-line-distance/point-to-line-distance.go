package pointtolinedistance

import (
	"math"

	"gitee.com/giserhan/turf-go/distance"
	"gitee.com/giserhan/turf-go/helpers"
	"gitee.com/giserhan/turf-go/helpers/constants/factors"
	rhumbdistance "gitee.com/giserhan/turf-go/rhumb-distance"
)

type Method string

const (
	PlanarMethod   Method = "planar"
	GeodesicMethod Method = "geodesic"
)

func PointToLineDistance(pt []float64, line [][]float64, units factors.Factor, method Method) (float64, error) {
	if len(line) < 2 {
		panic("坐标点数量至少为2")
	}
	distance := math.MaxFloat64
	p := pt
	for i := 0; i < len(line)-1; i++ {
		a := line[i]
		b := line[i+1]
		d := distanceToSegment(p, a, b, units, method)
		if d < distance {
			distance = d
		}
	}
	return helpers.ConvertLength(distance, factors.Degrees, units)
}

func distanceToSegment(p []float64, a []float64, b []float64, units factors.Factor, method Method) float64 {
	v := []float64{
		b[0] - a[0], b[1] - a[1],
	}
	w := []float64{
		p[0] - a[0], p[1] - a[1],
	}
	c1 := dot(w, v)
	if c1 <= 0 {
		return calcDistance(p, a, factors.Degrees, method)
	}
	c2 := dot(v, v)
	if c2 <= c1 {
		return calcDistance(p, b, factors.Degrees, method)
	}
	b2 := c1 / c2
	Pb := []float64{a[0] + b2*v[0], a[1] + b2*v[1]}
	return calcDistance(p, Pb, factors.Degrees, method)
}

func dot(u []float64, v []float64) float64 {
	return u[0]*v[0] + u[1]*v[1]
}
func calcDistance(a []float64, b []float64, units factors.Factor, method Method) float64 {
	if method == "planar" {
		return rhumbdistance.RhumbDistance(a, b, units)
	}
	return distance.Distance(a, b, units)
}
