package pointtolinedistance_test

import (
	"testing"

	"gitee.com/giserhan/turf-go/helpers/constants/factors"
	pointtolinedistance "gitee.com/giserhan/turf-go/point-to-line-distance"
)

func TestMain(t *testing.T) {
	// t.Fatal(helpers.Round(1.2324242, 2))
	jj, err := pointtolinedistance.PointToLineDistance([]float64{0, 0}, [][]float64{
		{1, 1},
		{-1, 1},
	}, factors.Feet, pointtolinedistance.GeodesicMethod)
	t.Fatal(jj, err)
}
