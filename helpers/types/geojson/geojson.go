package geojson

import (
	geojsontypes "gitee.com/giserhan/turf-go/helpers/types/geojson_types"
	geometrytypes "gitee.com/giserhan/turf-go/helpers/types/geometry_types"
	"gitee.com/giserhan/turf-go/helpers/types/types"
)

type GeoJSON struct {
	Type        geojsontypes.GeoJSONType `json:"type"` // types/geojson_types
	BBox        types.BBox               `json:"bbox"`
	Features    []Feature                `json:"features"`
	Coordinates any                      `json:"coordinates"`
	Geometries  []Geometry               `json:"geometries"`
	Geometry    Geometry                 `json:"geometry"`
	Properties  interface{}              `json:"properties"`
}

type Geometry struct {
	Type        geometrytypes.GeometryType `json:"type"` // types/geometry_types
	Coordinates any                        `json:"coordinates"`
	Geometries  []Geometry                 `json:"geometries"`
}
type Feature struct {
	Type       string      `json:"type"`
	Geometry   Geometry    `json:"geometry"`
	Properties interface{} `json:"properties"`
}

func Point(coordinates []float64, properties interface{}) *Feature {
	return &Feature{
		Type: "Feature",
		Geometry: Geometry{
			Type:        geometrytypes.Point,
			Coordinates: coordinates,
		},
		Properties: properties,
	}
}
func MultiPoint(coordinates [][]float64, properties interface{}) *Feature {
	return &Feature{
		Type: "Feature",
		Geometry: Geometry{
			Type:        geometrytypes.MultiPoint,
			Coordinates: coordinates,
		},
		Properties: properties,
	}
}
func LineString(coordinates [][]float64, properties interface{}) *Feature {
	return &Feature{
		Type: "Feature",
		Geometry: Geometry{
			Type:        geometrytypes.LineString,
			Coordinates: coordinates,
		},
		Properties: properties,
	}
}
func MultiLineString(coordinates [][][]float64, properties interface{}) *Feature {
	return &Feature{
		Type: "Feature",
		Geometry: Geometry{
			Type:        geometrytypes.MultiLineString,
			Coordinates: coordinates,
		},
		Properties: properties,
	}
}
func Polygon(coordinates [][][]float64, properties interface{}) *Feature {
	return &Feature{
		Type: "Feature",
		Geometry: Geometry{
			Type:        geometrytypes.Polygon,
			Coordinates: coordinates,
		},
		Properties: properties,
	}
}
func MultiPolygon(coordinates [][][][]float64, properties interface{}) *Feature {
	return &Feature{
		Type: "Feature",
		Geometry: Geometry{
			Type:        geometrytypes.MultiPolygon,
			Coordinates: coordinates,
		},
		Properties: properties,
	}
}
func GeometryCollection(geometries []Geometry, properties interface{}) *Feature {
	return &Feature{
		Type: "Feature",
		Geometry: Geometry{
			Type:       geometrytypes.GeometryCollection,
			Geometries: geometries,
		},
		Properties: properties,
	}
}
func FeatureCollection(features []Feature) *GeoJSON {
	return &GeoJSON{
		Features: features,
		Type:     geojsontypes.FeatureCollection,
	}
}
