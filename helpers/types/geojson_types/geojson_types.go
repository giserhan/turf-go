package geojsontypes

type GeoJSONType string

const (
	Geometry           GeoJSONType = "Geometry"
	Feature            GeoJSONType = "Feature"
	FeatureCollection  GeoJSONType = "FeatureCollection"
	Point              GeoJSONType = "Point"
	MultiPoint         GeoJSONType = "MultiPoint"
	LineString         GeoJSONType = "LineString"
	MultiLineString    GeoJSONType = "MultiLineString"
	Polygon            GeoJSONType = "Polygon"
	MultiPolygon       GeoJSONType = "MultiPolygon"
	GeometryCollection GeoJSONType = "GeometryCollection"
)
