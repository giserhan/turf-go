package geometrytypes

type GeometryType string

const (
	Point              GeometryType = "Point"
	MultiPoint         GeometryType = "MultiPoint"
	LineString         GeometryType = "LineString"
	MultiLineString    GeometryType = "MultiLineString"
	Polygon            GeometryType = "Polygon"
	MultiPolygon       GeometryType = "MultiPolygon"
	GeometryCollection GeometryType = "GeometryCollection"
)
