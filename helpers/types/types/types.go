package types

//[2]float64表示xy
type Position [2]float64

//[4]float表示[w,s,e,n]
type BBox [4]float64
