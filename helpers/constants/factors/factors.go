package factors

import (
	"math"

	"gitee.com/giserhan/turf-go/helpers/constants"
)

const (
	EarthRadius = constants.EarthRadius
)

type Factor float64

//factors
const (
	Centimeters   Factor = EarthRadius * 100
	Centimetres   Factor = EarthRadius * 100
	Degrees       Factor = 360 / (2 * math.Pi)
	Feet          Factor = EarthRadius * 3.28084
	Inches        Factor = EarthRadius * 39.37
	Kilometers    Factor = EarthRadius / 1000
	Kilometres    Factor = EarthRadius / 1000
	Meters        Factor = EarthRadius
	Metres        Factor = EarthRadius
	Miles         Factor = EarthRadius / 1609.344
	Millimeters   Factor = EarthRadius * 1000
	Millimetres   Factor = EarthRadius * 1000
	Nauticalmiles Factor = EarthRadius / 1852
	Radians       Factor = 1.0
	Yards         Factor = EarthRadius * 1.0936
)
