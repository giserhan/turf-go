package helpers

import (
	"errors"
	"math"

	"gitee.com/giserhan/turf-go/helpers/constants/factors"
)

// @Description 保留小数点后n位
//
// @Param num 浮点数
//
// @Param precision 保留位数
func Round(num float64, precision int) float64 {
	if precision < 0 {
		panic(errors.New("参数percision必须是非负整数"))
	}
	multiplier := math.Pow(10, float64(precision))
	return math.Round(num*multiplier) / multiplier
}
func Remain(num float64, numerator float64) float64 {
	factor := 1.0
	if num < 0 {
		factor = -1.0
	}
	result := 0.00
	num = math.Abs(num)
	for {
		if num > numerator {
			num -= numerator
			continue
		}
		result = num
		break
	}
	return result * factor
}

//@Description 弧度转角度
//
//@Param radians 弧度
func RadiansToDegrees(radians float64) float64 {
	degrees := Remain(radians, math.Pi*2)
	return (degrees * 180) / math.Pi
}

//@Description 角度转弧度
//
//@Param degrees 角度
func DegreesToRadians(degrees float64) float64 {
	radians := Remain(degrees, 360)
	return radians * math.Pi / 180
}

// @Description 弧度转长度
//
// @Param radians 弧度
//
// @Param units 返回结果单位对应的监测因子 factors
func RadiansToLength(
	radians float64,
	units factors.Factor, //factors
) float64 {
	return radians * float64(units)
}

//@Description 长度转弧度
//
//@Param distance 长度
//
//@Param units 单位
func LengthToRadians(
	distance float64,
	units factors.Factor,
) float64 {
	return distance / float64(units)
}

//@Description 长度转角度
//
//@Param distance 长度
//
//@Param units 单位
func LengthToDegrees(
	distance float64,
	units factors.Factor,
) float64 {
	return RadiansToDegrees(LengthToRadians(distance, units))
}

//@Description 方向角转方位角
//
//@Param bearing 方向角
func BearingToAzimuth(bearing float64) float64 {
	angle := Remain(bearing, 360)
	if angle < 0 {
		angle += 360
	}
	return angle
}

//@Description 长度单位换算
//
//@Param length 长度
//
//@Param originalUnit 长度单位
//
//@Param finalUnit 返回单位
func ConvertLength(length float64, originalUnit factors.Factor, finalUnit factors.Factor) (float64, error) {
	if length < 0 {
		return 0.0, errors.New("length must be a positive number")
	}
	return RadiansToLength(LengthToRadians(length, originalUnit), finalUnit), nil
}

func ConvertArea(
	area float64,
	originalUnit factors.Factor, finalUnit factors.Factor,
) (float64, error) {
	if !(area >= 0) {
		return 0, errors.New("area must be a positive number")
	}
	return (area / (float64(originalUnit) * float64(originalUnit))) * float64(finalUnit) * float64(finalUnit), nil
}
