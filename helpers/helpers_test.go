package helpers_test

import (
	"testing"

	"gitee.com/giserhan/turf-go/helpers"
	"gitee.com/giserhan/turf-go/helpers/constants/factors"
)

func TestMain(t *testing.T) {
	t.Fatal(helpers.ConvertArea(1000, factors.Meters, factors.Kilometers))
	// jj, _ := projection.ToWGS84(geojson.GeoJSON{})
	// t.Fatal(jj.Coordinates)
}
