package units

const (
	Meters        = "meters"
	Metres        = "metres"
	Millimeters   = "millimeters"
	Millimetres   = "millimetres"
	Centimeters   = "centimeters"
	Centimetres   = "centimetres"
	Kilometers    = "kilometers"
	Kilometres    = "kilometres"
	Acres         = "acres"
	Miles         = "miles"
	Nauticalmiles = "nauticalmiles"
	Inches        = "inches"
	Yards         = "yards"
	Feet          = "feet"
	Radians       = "radians"
	Degrees       = "degrees"
	Hectares      = "hectares"
)
