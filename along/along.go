package along

import (
	"gitee.com/giserhan/turf-go/bearing"
	"gitee.com/giserhan/turf-go/destination"
	measureDistance "gitee.com/giserhan/turf-go/distance"
	"gitee.com/giserhan/turf-go/helpers/constants/factors"
)

func Along(line [][]float64, distance float64, units factors.Factor) []float64 {
	travelled := 0.0
	pointlength := len(line)
	for i := 0; i < pointlength; i++ {
		if distance >= travelled && i == pointlength-1 {
			break
		} else if travelled >= distance {
			overshot := distance - travelled
			if overshot == 0 {
				return line[i]
			} else {
				direction := bearing.Bearing(line[i], line[i-1], false) - 180
				interpolated := destination.Destination(line[i], overshot, direction, units)
				return interpolated
			}
		} else {
			travelled += measureDistance.Distance(line[i], line[i+1], units)
		}
	}
	return line[pointlength-1]
}
