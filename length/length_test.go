package length_test

import (
	"testing"

	"gitee.com/giserhan/turf-go/helpers/constants/factors"
	"gitee.com/giserhan/turf-go/length"
)

func TestMain(t *testing.T) {
	// t.Fatal(helpers.Round(1.2324242, 2))
	jj := length.Length([][]float64{
		{
			117.02199699999998,
			30.529979,
			43.39193010412693,
		},
		{
			117.02295169399119,
			30.5299789965197,
			27.610149835265204,
		},
		{
			117.03154393988943,
			30.529978651970044,
			42.7294172948287,
		},
	}, factors.Meters)
	t.Fatal(jj)
}
