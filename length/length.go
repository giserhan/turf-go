package length

import (
	"gitee.com/giserhan/turf-go/distance"
	"gitee.com/giserhan/turf-go/helpers/constants/factors"
)

func Length(coords [][]float64, units factors.Factor) float64 {
	if len(coords) < 2 {
		return 0
	}
	length := 0.0
	for i := 1; i < len(coords); i++ {
		item := coords[i]
		pre := coords[i-1]
		length += distance.Distance(item, pre, units)
	}
	return length
}
