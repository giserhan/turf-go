### 仓库简介
Turf.js的golang实现，golang学习后的小试牛刀:)  
[Turf.js官网地址](http://turfjs.org/)

### 进度管理
- geojson格式支持
- 新增helpers包，新增函数RadiansToDegrees、DegreesToRadians、RadiansToLength、LengthToRadians、LengthToDegrees、BearingToAzimuth、ConvertLength、ConvertArea
- 新增函数distance、destination、bearing