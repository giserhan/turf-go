package rhumbdistance

import (
	"math"

	"gitee.com/giserhan/turf-go/helpers"
	"gitee.com/giserhan/turf-go/helpers/constants/factors"
)

func RhumbDistance(from []float64, to []float64, units factors.Factor) float64 {
	origin := from
	destination := to
	if destination[0]-origin[0] > 180 {
		destination[0] += -360
	} else {
		if origin[0]-destination[0] > 180 {
			destination[0] += 360
		}
	}
	distanceInMeters := calculateRhumbDistance(origin, destination)
	distance, err := helpers.ConvertLength(distanceInMeters, factors.Meters, units)
	if err != nil {
		panic(err)
	}
	return distance
}

func calculateRhumbDistance(origin []float64, destination []float64) float64 {
	earthRadius := 6371008.8
	radius := earthRadius
	R := radius
	phi1 := (origin[1] * math.Pi) / 180
	phi2 := (destination[1] * math.Pi) / 180
	DeltaPhi := phi2 - phi1
	DeltaLambda := (math.Abs(destination[0]-origin[0]) * math.Pi) / 180
	if DeltaLambda > math.Pi {
		DeltaLambda -= 2 * math.Pi
	}
	DeltaPsi := math.Log(math.Tan(phi2/2+math.Pi/4) / math.Tan(phi1/2+math.Pi/4))
	q := 0.0
	if math.Abs(DeltaPsi) > 10e-12 {
		q = DeltaPhi / DeltaPsi
	} else {
		q = math.Cos(phi1)
	}
	delta := math.Sqrt(DeltaPhi*DeltaPhi + q*q*DeltaLambda*DeltaLambda)
	dist := delta * R
	return dist
}
