package destination

import (
	"math"

	"gitee.com/giserhan/turf-go/helpers"
	"gitee.com/giserhan/turf-go/helpers/constants/factors"
)

func Destination(origin []float64, distance float64, bearing float64, units factors.Factor) []float64 {
	coordinates1 := origin
	longitude1 := helpers.DegreesToRadians(coordinates1[0])
	latitude1 := helpers.DegreesToRadians(coordinates1[1])
	bearingRad := helpers.DegreesToRadians(bearing)
	radians := helpers.LengthToRadians(distance, units)
	latitude2 := math.Asin(math.Sin(latitude1)*math.Cos(radians) +
		math.Cos(latitude1)*math.Sin(radians)*math.Cos(bearingRad))
	longitude2 := longitude1 +
		math.Atan2(math.Sin(bearingRad)*math.Sin(radians)*math.Cos(latitude1), math.Cos(radians)-math.Sin(latitude1)*math.Sin(latitude2))
	lng := helpers.RadiansToDegrees(longitude2)
	lat := helpers.RadiansToDegrees(latitude2)
	return []float64{
		lng, lat,
	}
}
