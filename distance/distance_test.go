package distance_test

import (
	"testing"

	"gitee.com/giserhan/turf-go/distance"
	"gitee.com/giserhan/turf-go/helpers/constants/factors"
)

func TestMain(t *testing.T) {
	// t.Fatal(helpers.Round(1.2324242, 2))
	jj := distance.Distance([]float64{
		117.02199699999998,
		30.529979}, []float64{
		117.02295169399119,
		30.5299789965197}, factors.Meters)
	t.Fatal(jj)
}
