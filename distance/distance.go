package distance

import (
	"math"

	"gitee.com/giserhan/turf-go/helpers"
	"gitee.com/giserhan/turf-go/helpers/constants/factors"
)

//计算两个经纬度点之间的距离
func Distance(from []float64, to []float64, units factors.Factor) float64 {
	dLat := helpers.DegreesToRadians(to[1] - from[1])
	dLon := helpers.DegreesToRadians(to[0] - from[0])
	lat1 := helpers.DegreesToRadians(from[1])
	lat2 := helpers.DegreesToRadians(to[1])
	a := math.Pow(math.Sin(dLat/2), 2) +
		math.Pow(math.Sin(dLon/2), 2)*math.Cos(lat1)*math.Cos(lat2)
	return helpers.RadiansToLength(2*math.Atan2(math.Sqrt(a), math.Sqrt(1-a)), units)
}
