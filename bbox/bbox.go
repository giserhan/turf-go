package bbox

import "math"

func BBox(coords [][]float64) []float64 {
	result := []float64{
		math.MaxFloat64,
		math.MaxFloat64,
		-math.MaxFloat64,
		-math.MaxFloat64,
	}
	for _, coord := range coords {
		if result[0] > coord[0] {
			result[0] = coord[0]
		}
		if result[1] > coord[1] {
			result[1] = coord[1]
		}
		if result[2] < coord[0] {
			result[2] = coord[0]
		}
		if result[3] < coord[1] {
			result[3] = coord[1]
		}
	}
	return result
}
