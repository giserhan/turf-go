package bbox_test

import (
	"testing"

	"gitee.com/giserhan/turf-go/bbox"
)

func TestMain(t *testing.T) {
	// t.Fatal(helpers.Round(1.2324242, 2))

	t.Fatal(bbox.BBox([][]float64{
		{-74, 40},
		{-78, 42},
		{-82, 35},
	}))
}
